# Discord Bingo Bot
## Installation

Python 3.5 or above and the necessary python packages are required to use the bot.

Necessary packages include:
    discord,
    pickle

You can install the packages with pip.

**execute setup.py**

**copy/paste** your discord bot/application token into bot_info.txt

## Use

User must edit and execute bingos.py to create bingo cards.  For more info read bingos.py
