from discord.ext.commands import Bot
import pickle

users = {}
winners = []
BOT_PREFIX = ("!")
client = Bot(command_prefix=BOT_PREFIX)

f = open('game_data/bot_info.txt', 'r')
TOKEN = f.readline().rstrip()
f.close()

with open("game_data/winners.txt", "r") as f1:
  for line in f1:
    winners.append(line.strip())


def load_obj(name):
    with open('game_data/' + name + '.pkl', 'rb') as f:
        return pickle.load(f)


bingo_cards = load_obj('bingos')


def check_bingo(marked):
    """Checks for a bingo given list of indices of marked squares."""
    # check vertical bingos
    column = 0
    for i in range(5):
        iprime = i
        for v in range(5):
            if iprime in marked:
                column += 1
            iprime += 5

        if column >= 5:
            return True
        else:
            column = 0

    # check horizontal bingos
    row = 0
    for i in range(5):
        iprime = i * 5
        for v in range(5):
            if (v+iprime) in marked:
                row += 1

        if row >= 5:
            return True
        else:
            row = 0

    # check diagonal bingos
    marked = set(marked)
    d1 = set([0, 6, 12, 18, 24])
    d2 = set([4, 8, 12, 16, 20])

    if d1.issubset(marked):
        return True
    if d2.issubset(marked):
        return True

    return False


@client.command(name='choose',
                description="Choose whose bingo to play.  Enter in their IRL name.  Example:  !choose Daniel",
                brief="Choose whose bingo to play.",
                pass_context=True)
async def choose(ctx):
    """Discord command.  Check discord command description with !help in discord."""
    username = ctx.message.author.name
    bingo = ctx.message.content[8:]
    card = bingo_cards[bingo]
    users.update({username: {'card': card, 'marked': []}})
    await client.say(str(users[username]['card'])[1:-1].replace("'", ''))


@client.command(name='list_cards',
                description="List all available bingo cards.  Example:  !list_cards",
                brief="List all available bingo cards.",
                aliases=['list', 'l'],
                pass_context=True)
async def list_cards(ctx):
    """Discord command.  Check discord command description with !help in discord."""
    cards = bingo_cards.keys()
    cards = str(cards)[11:-2].replace("'", '')
    await client.say(cards)


@client.command(name='mark',
                description="Mark a square.  Example:  !mark whole dang paragraph of square",
                brief="Mark a square.",
                aliases=['m'],
                pass_context=True)
async def mark(ctx):
    """Discord command.  Check discord command description with !help in discord."""
    arg = ctx.message.content[6:]
    username = ctx.message.author.name
    square = users[username]['card'].index(arg)
    users[username]['marked'].append(square)
    if check_bingo(users[username]['marked']) == True:
        await client.say('BINGO!\n'*5)
        winners.append(username)
        with open('game_data/winners.txt', 'a') as f:
            f.write("%s\n" % username)
    else:
        marked = [users[username]['card'][i] for i in users[username]['marked']]
        marked = str(marked)[1:-1].replace("'", '')
        await client.say('Marked Squares:\n' + marked)


@client.command(name='bingos',
                description="List all bingo winners.  Example:  !bingos",
                brief="List all bingo winners.",
                aliases=['who_has_a_bingo', 'winners'],
                pass_context=True)
async def bingos(ctx):
    """Discord command.  Check discord command description with !help in discord."""
    await client.say('Bingo winners are:\n' + str(winners)[1:-1].replace("'", ''))


# developers test/experiment command
# @client.command(name='test')
# async def test():
#     """Discord command.  Check discord command description with !help in discord."""
#     # global users
#     await client.say(users)


client.run(TOKEN)
